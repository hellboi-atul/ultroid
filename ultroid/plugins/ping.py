from datetime import datetime
import time
from telethon import events

@ultroid.on(events.NewMessage(pattern=f"{hndlr}ping", outgoing=True))
@ultroid.on(events.NewMessage(pattern=f"{hndlr}ping", incoming=True))
async def _(event):
    await event.edit("`Pong !`")
    if event.fwd_from:
        return
    start = datetime.now()
    end = datetime.now()
    ms = (end - start).microseconds / 1000
    await event.edit(f"**Pong !!** `{ms}ms` ")


@ultroid.on(events.NewMessage(pattern=f"{hndlr}pong", outgoing=True))
async def _(event):
    if event.fwd_from:
        return
    start = datetime.now()
    end = datetime.now()
    ms = (end - start).microseconds / 1000 * 2
    await event.edit(f"Ping! 🎾 {ms}ms ..")
