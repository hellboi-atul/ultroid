import glob
from pathlib import Path
from . import ultroid_bot
import logging
from telethon import TelegramClient
import telethon.utils
from ultroid.utils import *

async def istart(ult):
    await ultroid_bot.start(ult)
    ultroid_bot.me = await ultroid_bot.get_me() 
    ultroid_bot.uid = telethon.utils.get_peer_id(ultroid_bot.me)

async def bot_info(BOT_TOKEN):
    asstinfo = await asst.get_me()
    bot_name = asstinfo.username

logging.basicConfig(format='[%(levelname) 5s/%(asctime)s] %(name)s: %(message)s',
                    level=logging.WARNING)
                    
                    
ultroid_bot.asst = None
print("Initialising...")
if Var.BOT_TOKEN is not None:
    print("Starting Ultroid...")
    ultroid_bot.asst = TelegramClient("BOT_TOKEN",api_id=Var.API_ID,api_hash=Var.API_HASH).start(bot_token=Var.BOT_TOKEN)
    ultroid_bot.loop.run_until_complete(istart(Var.BOT_USERNAME))
    print("User Mode - Done")
    print("Done, startup completed")
else:
    print("Starting User Mode...")
    ultroid_bot.start()

path = "ultroid/plugins/*.py"
files = glob.glob(path)
for name in files:
    with open(name) as a:
        patt = Path(a.name)
        plugin_name = patt.stem
        load_plugins(plugin_name.replace(".py", ""))
        print (f"Ultroid - installed - {plugin_name}")

# for assistant
if Var.BOT_TOKEN != None:
    path = "ultroid/assistant/*.py"
    files = glob.glob(path)
    for name in files:
        with open(name) as a:
            patt = Path(a.name)
            plugin_name = patt.stem
            load_assistant(plugin_name.replace(".py", ""))
            print (f"Ultroid - Assistant - installed - {plugin_name}")


async def hehe():
    if Var.BOT_TOKEN:
        await ultroid_bot.asst.send_message(Var.LOG_CHANNEL, f"__**ULTROID Restarted**__\n**By User** [{ultroid_bot.me.first_name}](tg://user?id={ultroid_bot.uid})")
    else:
        await ultroid_bot.send_message(Var.LOG_CHANNEL, f"__**ULTROID Restarted**__")
ultroid_bot.loop.run_until_complete(hehe())
    
print("Ultroid has been deployed! Visit @TheUltroid for updates!!")

if __name__ == "__main__":
    ultroid_bot.run_until_disconnected()
